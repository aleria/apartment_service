const config = require("./config.json");
const express = require("express");
const { Apartment } = require("./utils/db_connexion");
const bearerToken = require("express-bearer-token");
const fetch = require("node-fetch");

const bunyan = require("bunyan");

var logger = bunyan.createLogger({
  name: "Apartement_logs",
  level: process.env.NODE_ENV != "production" ? "info" : "error",
  streams: [
    {
      path:
        process.env.NODE_ENV != "production"
          ? "/usr/app/logs/apartement.test.log"
          : "/usr/app/logs/apartement.production.log",
      type: "file",
    },
  ],
});

const App = express();
App.use(express.json());
App.use(bearerToken());

const findbyIds = require("./api/find_by_ids");
App.get(config.dev.BASE_ENDPOINT_IDS_ARRAY, (req, res) => {
  logger.info({ function: "findbyIds()", request: req.query });
  findbyIds(Apartment, req.query.a)
    .then((value) => {
      logger.info({
        function: "findApartement()",
        request: req.query,
        status_code: 200,
        query_result: value,
      });
      res.status(200).send(value);
    })
    .catch((err) => {
      logger.error({
        function: "findApartement()",
        request: req.query,
        status_code: 500,
        stack_trace: err,
      });
      res.status(500).send(err);
    });
});

const findApartement = require("./api/find_apartement");
App.get(config.dev.BASE_ENDPOINT, async (req, res) => {
  logger.info({ function: "findApartement()", request: req.query });

  const promise = findApartement(Apartment, req.query);
  promise
    .then((query_result) => {
      logger.info({
        function: "findApartement()",
        request: req.query,
        status_code: 200,
      });
      res.status(200).send(query_result);
    })
    .catch((err) => {
      logger.info({
        function: "findApartement()",
        request: req.query,
        status_code: 500,
        stack_trace: err,
      });
      res.status(500).send(err);
    });
});

const deleteApartment = require("./api/delete_apartment");
App.delete(config.dev.BASE_ENDPOINT_PLUS_ID, (req, res) => {
  const apartement_id = req.params.id;
  logger.info({
    function: "deleteApartement()",
    requested_id: apartement_id,
    endpoint: "/apartement/:id",
  });
  const response = deleteApartment(Apartment, apartement_id);
  response
    .then(() => {
      logger.info({
        function: "deleteApartement()",
        result: "Entry deleted",
        status_code: 204,
      });
      res.status(204).send("");
    })
    .catch((err) => {
      logger.fatal({
        function: "deleteApartement()",
        result: "Error",
        status_code: 500,
        stack_trace: err,
      });
      res.status(500).send(err);
    });
});

const getApartementById = require("./api/get_apartement_by_id");
App.get(config.dev.BASE_ENDPOINT_PLUS_ID, (req, res) => {
  const promise = getApartementById(Apartment, req.params.id)
  logger.info({
    function: "getPartementByID()",
    requested_id: req.params.id,
    endpoint: "/apartement/:id",
  });
  promise
    .then((query_result) => {
      logger.info({
        function: "getPartementByID()",
        requested_id: req.params.id,
        endpoint: "/apartement/:id",
        query_result: query_result,
        status_code: 200,
      });
      res.status(200).send(query_result);
    })
    .catch((err) => {
      console.log(err);
      logger.fatal({
        function: "getPartementByID()",
        requested_id: req.params.id,
        endpoint: "/apartement/:id",
        stack_trace: err,
        status_code: 500,
      });
      res.status(500).send(err);
    });
});

const updateApartment = require("./api/update_apartments");
App.put(config.dev.BASE_ENDPOINT_PLUS_ID, (req, res) => {
  const apartement_id = req.params.id;
  logger.info({
    function: "updateApartement()",
    requested_id: apartement_id,
    request_body: req.body,
    endpoint: "/apartement/:id",
  });
  const response = updateApartment(Apartment, apartement_id, req.body);
  response
    .then((value) => {
      if (typeof value == "string") {
        logger.info({
          function: "updateApartement()",
          requested_id: apartement_id,
          request_body: req.body,
          endpoint: "/apartement/:id",
          status_code: 400,
          query_result: value,
        });
        res.status(400).send("Bad request");
      } else {
        logger.info({
          function: "updateApartement()",
          requested_id: apartement_id,
          request_body: req.body,
          endpoint: "/apartement/:id",
          status_code: 200,
          query_result: value,
        });
        res.status(200).send(value);
      }
    })
    .catch((err) => {
      logger.fatal({
        function: "updateApartement()",
        requested_id: apartement_id,
        endpoint: "/apartement/:id",
        stack_trace: err,
        query: req.query,
        status_code: 500,
      });
      res.status(500).send(err);
    });
});

const insertApartment = require("./api/insert_apartment").insertApartment;
App.post(config.dev.BASE_ENDPOINT, (req, res) => {
  let response = insertApartment(Apartment, req.body);
  logger.info({
    function: "insertApartment()",
    request_body: req.body,
    endpoint: "/apartement/",
  });
  response
    .then((value) => {
      if (typeof value == "string") {
        logger.info({
          function: "insertApartment()",
          request_body: req.body,
          endpoint: "/apartement/",
          status: 400,
          query_result: value,
        });
        res.status(400).send("Bad request");
      } else {
        logger.info({
          function: "insertApartment()",
          request_body: req.body,
          endpoint: "/apartement/",
          status: 201,
          query_result: value,
        });
        res.status(201).send(value);
      }
    })
    .catch((err) => {
      logger.fatal({
        function: "insertApartment()",
        request_body: req.body,
        endpoint: "/apartement/",
        status: 500,
        stack_trace: err,
      });
      res.status(500).send(err);
    });
});

if (process.env.NODE_ENV != "test") {
  App.listen(config.dev.PORT, () => {
    console.log("Apartments service up and running");
  });
}

module.exports = App;
