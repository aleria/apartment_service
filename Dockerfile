FROM node:alpine

ENV NODE_ENV prod

RUN mkdir -p /usr/app/logs
RUN touch /usr/app/logs/apartement.production.log
WORKDIR /usr/app

COPY package*.json ./ 
RUN npm install 

COPY . . 

EXPOSE 3000 

CMD npm start 