const deleteApartment = async (model,id) => {
  return model.findByIdAndRemove(id)
};

module.exports = deleteApartment;
