const findbyIds = async (model, ids)=> { 
   return  model.find({'_id':{$in: ids }}).sort({_id: 1})
}

module.exports = findbyIds