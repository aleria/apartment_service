const getApartements = (model, req, res) => {
  model.find()
    .then((values) => {
      res.json(values);
    })
    .catch((err) => {
      throw err;
    });
};

module.exports = getApartements;