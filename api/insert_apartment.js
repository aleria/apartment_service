const errors = {
  MISSING_USER_ID: "USER ID is missing",
  MISSING_STREET_NUMBER: "street number is missing",
  STREET_NAME_MISSING: "Stret name is missing",
  ZIP_CODE_IS_MISSING: "zip code is mising",
  TOWN_MISSING: "town is missing",
  SURFAC_MISSING: "Surface is missing",
  LABEL_MISSING: "Label is missing",
  AVAILABILITY_MISSING: "availability is missing",
};

const insertApartment = async (model, body) => {
  const data = {
    address: {
      street_no: body.address.street_no,
      street_name: body.address.street_name,
      zip_code: body.address.zip_code,
      town: body.address.town,
    },
    surface: body.surface,
    label: body.label,
    user_id: body.user_id,
    is_available: body.is_available,
  };
  const DATA_CHECK = chekcValues(data);
  if (DATA_CHECK != null) return DATA_CHECK;

  const new_apartment = new model(data);
  const response = new_apartment.save();
  return response;
};

const chekcValues = (object) => {
  if (object.address.street_no == null) return errors.MISSING_STREET_NUMBER;
  else if (object.user_id == null) return errors.MISSING_USER_ID;
  else if (object.surface == null) return errors.SURFAC_MISSING;
  else if (object.label == null) return errors.LABEL_MISSING;
  else if (object.is_available == null) return errors.AVAILABILITY_MISSING;
  else if (object.address.street_name == null)
    return errors.STREET_NAME_MISSING;
  else if (object.address.zip_code == null) return errors.ZIP_CODE_IS_MISSING;
  else if (object.address.town == null) return errors.TOWN_MISSING;
  else return null;
};

module.exports = {insertApartment, errors,chekcValues};
