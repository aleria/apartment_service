const chekcValues = require("./insert_apartment").chekcValues;
const updateApartment = async (model, id, body) => {
  const DATA = {
    address: {
      street_no: body.address.street_no,
      street_name: body.address.street_name,
      zip_code: body.address.zip_code,
      town: body.address.town,
    },
    surface: body.surface,
    label: body.label,
    user_id: body.user_id,
    is_available: body.is_available,
  };
  const DATA_CHECK = chekcValues(DATA);
  if (DATA_CHECK != null) return DATA_CHECK;

  return model.findByIdAndUpdate(id, DATA,{new: true});
};

module.exports = updateApartment;
