const { ObjectID } = require("bson");

const Apartment = {
  address: {
    street_no: {
      type: Number,
      require: false,
    },
    street_name: {
      type: String,
      require: false,
    },
    zip_code: {
      type: Number,
      require: false,
    },
    town: {
      type: String,
      require: false,
    },
  },
  surface: {
    type: Number,
    require: true,
  },
  label: {
    type: String,
    require: false,
  },
  user_id: {
    type: ObjectID,
    require: true,
  },
  is_available: {
    type: Boolean,
    require: true,
  },
};

module.exports = Apartment;
