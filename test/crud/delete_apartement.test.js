const insertNewApartement = require("../../api/insert_apartment").insertApartment;
const deleteApartement = require("../../api/delete_apartment");
const { Apartment } = require("../../utils/db_connexion");
var expect = require("chai").expect;

describe("A row will be added and removed in order to test delete function", () => {

  it("Insert a model no error expected", (done) => {
    let DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        deleteApartement(Apartment, value._id)
          .then((delete_apartement_result) => {
            expect(delete_apartement_result).to.have.property('_id')
            done();
          })
          .catch((err) => done(err));
      })
      .catch((err) => done(err));
  });

});
