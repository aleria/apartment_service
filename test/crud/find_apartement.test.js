const insertNewApartement = require("../../api/insert_apartment").insertApartment;
const findApartement = require("../../api/find_apartement");
const { Apartment } = require("../../utils/db_connexion");
var expect = require("chai").expect;
const assert = require("chai").assert;

describe("Will automaticly add an Apartement with user ID 6042609980217c00147c39c1 and reselect it ", () => {
  
  it("Will return empty array or array of object containing Apartement",  (done) => {
    let DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,

    };

    insertNewApartement(Apartment, DATA)
      .then((val) => {
        findApartement(Apartment, {user_id : "6042609980217c00147c39c1"}).then(
          (slected) => {
            expect(slected).to.be.a('array')
            done();
          }
        ).catch(err=> done(err));
      })
      .catch((err) => {
        done(err);
      });

  });

});
