const insertNewApartement = require("../../api/insert_apartment").insertApartment;
const erros = require("../../api/insert_apartment").errors;
const { Apartment } = require("../../utils/db_connexion");
var expect = require("chai").expect;
const assert = require("chai").assert;

describe("Inserting a valid data form no errors expected", (done) => {
  it("insertNewApartement() All fields present", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        assert.notEqual(value._id, null);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });

  it("insertNewApartement() User ID Missing ", (done) => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.MISSING_USER_ID);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("insertNewApartement() Missing street number", async () => {
    const DATA = {
      address: {
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.MISSING_STREET_NUMBER);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });
  
  it("insertNewApartement() missing STREET NAME MISSING", async () => {
    const DATA = {
      address: {
        street_no: 300,
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.STREET_NAME_MISSING);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });
  
  it("insertNewApartement() missing zip code", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.ZIP_CODE_IS_MISSING);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });

  it("insertNewApartement() Town missing", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.TOWN_MISSING);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });

  it("insertNewApartement() Surface is missing", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.SURFAC_MISSING);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });

  it("insertNewApartement() missing label", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.LABEL_MISSING);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });

  it("insertNewApartement() missing isAvailable", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        expect(value).to.equal(erros.AVAILABILITY_MISSING);
        done()
      })
      .catch((err) => {
        done(err)
      });
  });
});


