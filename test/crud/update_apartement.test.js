const insertNewApartement = require("../../api/insert_apartment")
  .insertApartment;
const updateApartment = require("../../api/update_apartments");
const erros = require("../../api/insert_apartment").errors;
const { Apartment } = require("../../utils/db_connexion");
var expect = require("chai").expect;
const assert = require("chai").assert;

describe("Inserting a valid data form no errors expected", (done) => {

  it("insertNewApartement() All fields present for testing updateApartement with all fields present", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertNewApartement(Apartment, DATA)
      .then((value) => {
        assert.notEqual(value._id, null);
        const NEW_DATA = {
          address: {
            street_no: 300,
            street_name: "This will be the updated Values",
            zip_code: 2222,
            town: "This will be the updated Values",
          },
          surface: 20.21,
          label: "This will be the updated Values",
          user_id: "6042609980217c00147c39c1",
          is_available: true,
        };
        updateApartment(Apartment,value._id, NEW_DATA)
          .then((updated_value) => {
            assert.notEqual(updated_value, null);
            assert.equal(updated_value._id, value._id);
            assert.notEqual(updated_value.street_name, value.street_name);
          })
          .catch((err) => done(err));
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() User ID Missing ", (done) => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      is_available: true,
    };
    updateApartment(Apartment,0,DATA)
      .then((value) => {
        expect(value).to.equal(erros.MISSING_USER_ID);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() Missing street number", async () => {
    const DATA = {
      address: {
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    updateApartment(Apartment,0,DATA)
      .then((value) => {
        expect(value).to.equal(erros.MISSING_STREET_NUMBER);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() missing STREET NAME MISSING", async () => {
    const DATA = {
      address: {
        street_no: 300,
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    updateApartment(Apartment,0,DATA)
      .then((value) => {
        expect(value).to.equal(erros.STREET_NAME_MISSING);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() missing zip code", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    updateApartment(Apartment, DATA,0)
      .then((value) => {
        expect(value).to.equal(erros.ZIP_CODE_IS_MISSING);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() Town missing", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    updateApartment(Apartment,0,DATA)
      .then((value) => {
        expect(value).to.equal(erros.TOWN_MISSING);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() Surface is missing", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    updateApartment(Apartment,0, DATA)
      .then((value) => {
        expect(value).to.equal(erros.SURFAC_MISSING);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() missing label", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    updateApartment(Apartment,0,DATA)
      .then((value) => {
        expect(value).to.equal(erros.LABEL_MISSING);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("updateApartement() missing isAvailable", async () => {
    const DATA = {
      address: {
        street_no: 300,
        street_name: "req.body.street.gym",
        zip_code: 2222,
        town: "req..town",
      },
      surface: 20.21,
      label: "req..label",
      user_id: "6042609980217c00147c39c1",
    };
    updateApartment(Apartment,0,DATA)
      .then((value) => {
        expect(value).to.equal(erros.AVAILABILITY_MISSING);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});
