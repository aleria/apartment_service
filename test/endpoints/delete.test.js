const request = require("supertest");
const App = require("../../App");
const config = require("../../config.json");
const insertApartment = require("../../api/insert_apartment").insertApartment;
const { Apartment } = require("../../utils/db_connexion");
const assert = require("chai").assert;

describe("this test orchestration will insert and delete a record using Delete /apartements/:id route", () => {
  it("Providing a functional example no error expected", (done) => {
    const DUMMY_DATA = {
      address: {
        street_no: 200,
        street_name: "Testing get endpoints street name",
        zip_code: 2222,
        town: "Testing get endpoints town",
      },
      surface: 123,
      label: "Testing_get_endpoints_Label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertApartment(Apartment, DUMMY_DATA)
      .then((value) => {
        assert.notEqual(null, value);
        assert.isObject(value);
        request(App)
          .delete(config.dev.BASE_ENDPOINT + "/" + value._id)
          .expect(204)
          .end(function (err, res) {
            if (err) return done(err);
            done();
          });
      })
      .catch((err) => done(err));
  });


  it("Providing a non functional example with a malformed ID and expecting 500 error", (done) => {
    const DUMMY_DATA = {
      address: {
        street_no: 200,
        street_name: "Testing get endpoints street name",
        zip_code: 2222,
        town: "Testing get endpoints town",
      },
      surface: 123,
      label: "Testing_get_endpoints_Label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertApartment(Apartment, DUMMY_DATA)
      .then((value) => {
        assert.notEqual(null, value);
        assert.isObject(value);
        request(App)
          .delete(config.dev.BASE_ENDPOINT + "/28djéç")
          .expect(500)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .end(function (err, res) {
            if (err) return done(err);
            assert.notEqual(res.body, null);
            assert.isObject(res.body)
            done();
          });
      })
      .catch((err) => done(err));
  });
});
