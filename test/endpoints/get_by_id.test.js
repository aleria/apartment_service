const request = require("supertest");
const App = require("../../App");
const config = require("../../config.json");
const insertApartment = require("../../api/insert_apartment").insertApartment;
const { Apartment } = require("../../utils/db_connexion");
const assert = require("chai").assert;

describe("In this orchestration a value will be added and selected using path param /apartement/:id", () => {
  it("Insert a new value and sending get request to reselect it ", (done) => {
    const DUMMY_DATA = {
      address: {
        street_no: 200,
        street_name: "Testing get endpoints street name",
        zip_code: 2222,
        town: "Testing get endpoints town",
      },
      surface: 123,
      label: "Testing_get_endpoints_Label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertApartment(Apartment, DUMMY_DATA)
      .then((value) => {
        assert.notEqual(value._id, null);
        request(App)
          .get(config.dev.BASE_ENDPOINT + "/" + value._id)
          .expect(200)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .end(function (err, res) {
            if (err) return done(err);
            assert.notEqual(res.body, null);
            assert.isObject(res.body);
            assert.equal(res.body._id, value._id);
            done();
          });
      })
      .catch((err) => done(err));
  });

  it("This request will send an inexistant ID in path params and will be expecting 204 no content", (done) => {
    request(App)
      .get(config.dev.BASE_ENDPOINT + "/" + "6042609980217c00147c39c1")
      .expect(200)
      .set("Accept", "application/json")
      .end(function (err, res) {
        if (err) return done(err);
        assert.notEqual(res.body, {});

        done();
      });
  });
});
