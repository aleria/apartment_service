const request = require("supertest");
const App = require("../../App");
const config = require("../../config.json");
const insertApartment = require("../../api/insert_apartment").insertApartment;
const { Apartment } = require("../../utils/db_connexion");
const assert = require("chai").assert;

describe("Get route /apartements", () => {
  it("Inserting a single value to ensure it precense in the DB during request simulation", (done) => {
    const DUMMY_DATA = {
      address: {
        street_no: 200,
        street_name: "Testing get endpoints street name",
        zip_code: 2222,
        town: "Testing get endpoints town",
      },
      surface: 123,
      label: "Testing_get_endpoints_Label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertApartment(Apartment, DUMMY_DATA)
      .then((value) => {
        assert.notEqual(value, null);
        done();
      })
      .catch((err) => done(err));
  });

  it("Test get no querry params  ", (done) => {
    request(App)
      .get(config.dev.BASE_ENDPOINT)
      .expect(200)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .end(function (err, res) {
        assert.isArray(res.body);
        assert.isNotEmpty(res.body);
        if (err) return done(err);
        done();
      });
  });

  it("Test get endpoint with multiple non nested attribute -Label, Surface, user_id-", (done) => {
    request(App)
      .get(
        config.dev.BASE_ENDPOINT +
          "?user_id=6042609980217c00147c39c1&label=Testing_get_endpoints_Label&surface=123"
      )
      .expect(200)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .end((err, res) => {
        if (err) return done(err);
        assert.notEqual(res.body, null);
        assert.isArray(res.body);
        assert.isNotEmpty(res.body);
        done();
      });
  });

  it("Test get endpoint with wrong querries params to ensure 204 not found result", (done) => {
    request(App)
      .get(config.dev.BASE_ENDPOINT + "?labl=inexistant_label&surfce=00")
      .expect(200)
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) return done(err);
        assert.isArray(res.body);
        assert.isEmpty(res.body);
        done();
      });
  });
});
