const request = require("supertest");
const App = require("../../App");
const config = require("../../config.json");
const assert = require("chai").assert;

describe("This test orchestration will test different output for Post /apartements", () => {
  it("Test insertion Post route /apartements expecting 201 created result", (done) => {
    request(App)
      .post(config.dev.BASE_ENDPOINT)
      .send({
        address: {
          street_no: 300,
          street_name: "req.body.street.gym",
          zip_code: 2222,
          town: "req..town",
        },
        surface: 20.21,
        label: "req..label",
        user_id: "6042609980217c00147c39c1",
        is_available: true,
      })
      .expect(201)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .end(function (err, res) {
        if (err) return done(err);
        assert.notEqual(null, res.body);
        done();
      });
  });

  it("Test insertion Post route /apartements with a wrong user_id params in req body to ensure 500 internal server error", (done) => {
    request(App)
      .post(config.dev.BASE_ENDPOINT)
      .send({
        address: {
          street_no: 300,
          street_name: "req.body.street.gym",
          zip_code: 2222,
          town: "req..town",
        },
        surface: 20.21,
        label: "req..label",
        user_id: "604260997c39c1",
        is_available: true,
      })
      .expect(500)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .end((err, res) => {
        if (err) return done(err);

        assert.notEqual(res.body.message, null);
        assert.equal(
          res.body.message,
          'Apartment validation failed: user_id: Cast to ObjectId failed for value "604260997c39c1" at path "user_id"'
        );
        done();
      });
  });

  it("Test insertion Post route /apartements with  missing params in body request expecting 400 Bad request", (done) => {
    request(App)
      .post(config.dev.BASE_ENDPOINT)
      .send({
        address: {
          street_no: 300,
          street_name: "req.body.street.gym",
          zip_code: 2222,
          town: "req..town",
        },
        surface: 20.21,
        label: "req..label",
        is_available: true,
      })
      .expect(400)
      .set("Accept", "application/json")
      .expect("Content-Type", /text/)
      .end((err, res) => {
        if (err) return done(err);
        assert.equal(res.text, "Bad request");
        done();
      });
  });
});
