const request = require("supertest");
const App = require("../../App");
const config = require("../../config.json");
const insertApartment = require("../../api/insert_apartment").insertApartment;
const { Apartment } = require("../../utils/db_connexion");
const assert = require("chai").assert;

describe("During this orchestration value will be inserted and update using Put /apartements/:id route", () => {
  it("Providing a functional exemple expecting 200 and object response", (done) => {
    const DUMMY_DATA = {
      address: {
        street_no: 200,
        street_name: "Testing get endpoints street name",
        zip_code: 2222,
        town: "Testing get endpoints town",
      },
      surface: 123,
      label: "Testing_get_endpoints_Label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };
    insertApartment(Apartment, DUMMY_DATA)
      .then((value) => {
        assert.notEqual(value._id, null);

        request(App)
          .put(config.dev.BASE_ENDPOINT + "/" + value._id)
          .send({
            address: {
              street_no: 200,
              street_name: "Testing get endpoints street name",
              zip_code: 2222,
              town: "Testing get endpoints town",
            },
            surface: 5334,
            label: "updated label",
            user_id: "6042609980217c00147c39c1",
            is_available: false,
          })
          .expect(200)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .end(function (err, res) {
            if (err) return done(err);
            assert.notEqual(res.body, null);
            assert.isObject(res.body);
            assert.equal(res.body._id, value._id);
            assert.notEqual(res.body.surface, value.surface);
            assert.notEqual(res.body.label, value.label);
            assert.notEqual(res.body.is_available, value.is_available);
            assert.equal(res.body._id, value._id);
            done();
          });
      })
      .catch((err) => done(err));
  });

  it("Providing a non functional body request missing various params  expecting 400 and error message", (done) => {
    const DUMMY_DATA = {
      address: {
        street_no: 200,
        street_name: "Testing get endpoints street name",
        zip_code: 2222,
        town: "Testing get endpoints town",
      },
      surface: 123,
      label: "Testing_get_endpoints_Label",
      user_id: "6042609980217c00147c39c1",
      is_available: true,
    };

    insertApartment(Apartment, DUMMY_DATA)
      .then((value) => {
        assert.notEqual(value._id, null);
        request(App)
          .put(config.dev.BASE_ENDPOINT + "/" + value._id)
          .send({
            address: {
              street_no: 200,
              street_name: "Testing get endpoints street name",
              zip_code: 2222,
              town: "Testing get endpoints town",
            },
            surface: 5334,
            is_available: false,
          })
          .expect(400)
          .set("Accept", "application/json")
          .expect("Content-Type", /text/)
          .end(function (err, res) {
            if (err) return done(err);
            assert.notEqual(res.text, null);
            assert.equal(res.text, "Bad request");
            done();
          });
      })
      .catch((err) => done(err));
  });

  it("Providing  functional body request and with malformed objectID in path param  expecting 500 internal server error", (done) => {
    request(App)
      .put(config.dev.BASE_ENDPOINT + "/Adaj213231")
      .send({
        address: {
          street_no: 200,
          street_name: "Testing get endpoints street name",
          zip_code: 2222,
          town: "Testing get endpoints town",
        },
        label: "Testing_get_endpoints_Label",
        user_id: "6042609980217c00147c39c1",
        surface: 5334,
        is_available: false,
      })
      .expect(500)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .end(function (err, res) {
     
        if (err) return done(err);
        assert.notEqual(res.body, null);
        assert.isObject(res.body);
        done();
      });
  });
});
