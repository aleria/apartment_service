const config = require("../config.json");
const mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);
const Apartment_model = require("../models/Apartment");

mongoose.model(config.dev.COLLECTION_NAME, Apartment_model);
  mongoose
    .connect(config.dev.DB_CONNEXION_STRING, { useNewUrlParser: true , useUnifiedTopology: true  })
    .then(() => {
      () => {
        console.log("apartment db connected");
      };
    })
    .catch((err) => {
      throw err;
    });

const Apartment = mongoose.model(config.dev.COLLECTION_NAME);
module.exports = { Apartment };
